package ru.sdnj.workfront.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 */
public class UploadStatusResponse {
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";

	@JsonProperty("status")
	private String status;
	@JsonProperty("error")
	private String error;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
