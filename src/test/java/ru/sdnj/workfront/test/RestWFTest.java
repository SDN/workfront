package ru.sdnj.workfront.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.feature.LoggingFeature;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

class RestWFTest {

	private static final String USER = "pifdemo2";
	private static final String URI = "http://sdnj-test.mircloud.us/rest/api/";
	private static final String URI1 = "http://localhost:8080/rest/api/";
	private static final String pk = "604c5d2c-6bbc-4375-aec1-4372195538d3";

	@Test
	void test() throws Exception {
		ClientBuilder builder = ClientBuilder.newBuilder()
				.register(new JacksonJsonProvider())
				.register(LoggingFeature.class);

		Client client = builder.build();

		//addUser(client);
		//login(client);

		client.register(new ClientRequestFilter() {

			@Override
			public void filter(ClientRequestContext context) throws IOException {
				context.getHeaders().add("username", USER);
				context.getHeaders().add("apiKey", pk);
			}
		});

		files(client);
		upload(client);
		files(client);
	}

	private void addUser(Client client) throws IOException {
		WebTarget target = getApi(client).path("accounts/createAccount");

		Map<String, String> entity = new HashMap<String, String>();
		entity.put("name", USER);
		entity.put("password", pk);
		entity.put("apiKey", pk);

		Response response = target.request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.json(entity));
	}

	private void login(Client client) throws IOException {
		WebTarget target = getApi(client).path("accounts/findAccounts");

		Response response = target
				.queryParam("name", USER)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.get();
		//.post(Entity.json(entity));
	}

	private void files(Client client) throws IOException {
		Response response = getApi(client).path("files")
				.request(MediaType.APPLICATION_JSON_TYPE)
				.get();
		List files = response.readEntity(List.class);
		files.forEach(System.out::println);
	}

	private void upload(Client client) throws IOException {
		String filename = "problem_houston20200217a.jpg";
		upload(client, filename);
		filename = "problem_houston_uploadFail200.jpg";
		upload(client, filename);
		filename = "problem_houston_uploadFail500.jpg";
		upload(client, filename);
		filename = "problem_houston_uploadFail.jpg";
		upload(client, filename);
		filename = "problem_houston_uploadInitFail500.jpg";
		upload(client, filename);
		filename = "problem_houston_uploadInitFail.jpg";
		upload(client, filename);

	}

	private void upload(Client client, String filename) throws IOException {
		Map<String, String> entity = new HashMap<String, String>();

		Response response = getApi(client).queryParam("parentId", "1")
				.queryParam("filename", filename)
				.queryParam("documentId", "5e41bb92001226ec98abcaf486e36882")
				.queryParam("documentVersionId", "5e4a7340000e5b49f97375bf07cfda40")
				.path("uploadInit")
				.request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.json(entity));
		//print(response);

		JsonNode json = null;
		try {
			json = response.readEntity(JsonNode.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (json == null || !json.hasNonNull("id")) {
			return;
		}
		String id = json.get("id").asText();

		Form form = new Form();
		form.param("id", id);
		response = getApi(client).path("upload").request(MediaType.APPLICATION_JSON_TYPE)
				.put(Entity.form(form));
	}

	private void print(Response response) throws IOException {
		System.out.println();
		response.getHeaders().entrySet().forEach(System.out::println);
		System.out.println(response.getEntity());
		try (InputStream readEntity = response.readEntity(InputStream.class);) {
			byte[] b = new byte[1024];
			readEntity.read(b);
			System.out.println(new String(b));
		}
	}

	private WebTarget getApi(Client client) {
		return client.target(URI);
	}

}
